import React from 'react';
import { Box, Text, Card  } from 'grommet';

function getAge(date) {
    var today = new Date();
    var birthDate = new Date(date);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

const About = (props) => {
    return(
    <>
        <Box justify="center" align="center">
            <Card style={{padding: 10, marginLeft:"20", marginRight:"20"}} width="80%">
                <Text width="justify" textAlign="center" justify="center" size="1em">
                    {getAge(new Date('1994/10/31'))} years old
                </Text>
                <Text width="justify" textAlign="center" justify="center" size="1em">
                    Currently studying for a masters degree in software development 
                </Text>
                <Text width="justify" textAlign="center" justify="center" size="1em">
                    Avid videogames enjoyer
                </Text>
                <Text width="justify" textAlign="center" justify="center" size="1em">
                    Musician/Composer during my free time
                </Text>
                <Text width="justify" textAlign="center" justify="center" size="1em">
                    Energy drinks addict
                </Text>
                <Text width="justify" textAlign="center" justify="center" size="1em">
                    Will crack the worst jokes at the best of times
                </Text>
                <Text width="justify" textAlign="center" justify="center" size="1em">
                    Over 5GB of memes saved in my phone
                </Text>
            </Card>
        </Box>
    </>
    )
}

export default About;