import React from 'react';
import { Box } from 'grommet';
import TechContainer from './tech/techContainer';
import XpContainer from './xp/xpContainer';
import About from './about/about';



const Data = (props) => {
    function renderSwitch(param){
        switch(param) {
            case 'knowledge': return(<TechContainer theme={props.theme}/>);
            case 'experience': return(<XpContainer theme={props.theme}/>);
            case 'other': return(<About theme={props.theme}/>);
            default: return(<About theme={props.theme}/>);
        }
    }

    return(
    <Box theme={props.theme} fill flex>
       {renderSwitch(props.category)}
    </Box>
    );
}

export default Data;