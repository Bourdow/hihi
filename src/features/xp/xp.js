import React from 'react';
import { Card, CardBody, CardHeader, Text } from 'grommet';

const Xp = (props) => {
    
    return(
    <>
      <Card style={{padding: 10, marginLeft:"20", marginRight:"20"}} width="80%">
          <CardHeader direction="row" style={{paddingBottom:"10"}}>
            <Text style={{width:"33.3%", textAlign:"center", fontStyle:"italic"}}>
              {props.year}
            </Text>
            <Text style={{width:"33.3%", textAlign:"center", fontWeight:"bold"}}>
              {props.title}
            </Text>
            <Text style={{width:"33.3%", textAlign:"center", fontStyle:"italic"}}>
              {props.employer}
            </Text>
          </CardHeader>
          <CardBody pad={{horizontal: 'medium'}}>
            {props.children}
          </CardBody>
      </Card>
    </>
    )
}

export default Xp;