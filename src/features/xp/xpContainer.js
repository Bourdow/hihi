import React from 'react';
import { Box } from 'grommet';
import Xp from './xp';

const XpContainer = (props) => {
    
    return(
    <Box fill style={{display: "flex", flexDirection: "column", justifyContent: "space-around"}} align="center" justify="center">
        <Xp year="Since 2019" title="Fullstack Java/J2E Developer" employer="MiPiH">
            Developing modules for a custom ERP aimed at health structures such as hospitals and cancer clinics
        </Xp>
        <Xp year="2019" title="Fullstack Web Developer" employer="Lalachante">
            Implementing a payment solution, and a user profile page in order to rent a music studio or provide additional services
        </Xp>
        <Xp year="2017-2018" title="On-Site Agent - Tourism" employer="Eurotunnel">
            Managing the trafic flow and informing customers across the terminal (On-the-fly French/English, customer service skills)
        </Xp>
    </Box>
    )
}

export default XpContainer;