import React from 'react';
import { Box } from 'grommet';
import Technology from './technology'

const TechContainer = (props) => {
    
    return(
        <Box pad={{horizontal:'xlarge'}} align="center" justify="center" direction="row">
            <Technology techName="Java" imgLink="img/java.png"/>
            <Technology techName="SQL" imgLink="img/sql.svg"/>
            <Technology techName="Hibernate" imgLink="img/hibernate.png"/>
            <Technology techName="Javascript" imgLink="img/js.svg"/>
            <Technology techName="React" imgLink="img/react.svg"/>
            <Technology techName="Angular" imgLink="img/angular.png"/>
            <Technology techName="Git" imgLink="img/git.png"/>
        </Box>
    )
}


export default TechContainer;