import React from 'react';
import { Image, Card } from 'grommet';

const Technology = (props) => {
    
    return(
    <Card height="small" justify="center" style={{marginLeft:"4px", marginRight:"4px"}}>
        <Image style={{width:"100%", maxWidth:"150px"}} align="center" justify="center" src={props.imgLink} />
    </Card>

    )
}

export default Technology;