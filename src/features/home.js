import React from 'react';
import { Grommet, Box, Text, Button, Avatar } from 'grommet';
import Data from './data';
import './styles.css';

const theme = {
    hover: {
        background: {
            opacity:"medium"
          }
    },
    global: {
      colors: {
        brand: '#000000'
      },
      font: {
        family: 'Roboto',
        size: '1.5em',
        height: '1.6em',
      },
    },
  };
const Home = () => {
    const [category, setCategory]= React.useState();
    return(
    <Grommet theme={theme} fill>
        <Box fill align="center" direction="column" justify="center">
            <Avatar animation={{ type:"fadeIn" }} size="200px" src="img/heyho.jpg" />
            <Box animation={{type:"slideUp"}}>
                <Text textAlign="center" size="2em">Hugo BOURDEAUD'HUI</Text>
                <Text textAlign="center" size="1em" style={{fontStyle:"italic"}}>
                    <Text size="1em" color="orange">{"{ "}</Text>
                    Java/J2E fullstack developer
                    <Text size="1em" color="orange">{" }"}</Text>
                </Text>
            </Box>
        </Box>
        <Box full style={{background: "linear-gradient(to bottom right, white, light blue)"}}>
            <ul style={{display: 'flex', flexDirection: 'row', alignItems: 'center', "list-style":'none'}}>
                <Button primary hoverIndicator style={{height:"auto", width:"33.3%", textAlign:"center"}} onClick={() => {setCategory('other')}} label="ABOUT ME" />
                <Button primary hoverIndicator style={{height:"auto", width:"33.3%", textAlign:"center"}} onClick={() => setCategory('knowledge')} label="KNOWLEDGE"/>
                <Button primary hoverIndicator style={{height:"auto", width:"33.3%", textAlign:"center"}} onClick={() => setCategory('experience')} label="EXPERIENCE"/>
            </ul>
            <Data category={category} theme={theme} />
        </Box>
        <Box background="dark-2" pad={{ horizontal: 'medium', vertical: 'small' }} justify="end" style={{position:"absolute",left:0,bottom:0,right:0}}>
            <Text size="small" style={{textAlign:"right"}}>
                © {new Date().getFullYear()} Copyright lmao
            </Text>
        </Box>
    </Grommet>
    )
}

export default Home;